package com.apitechu2.apitechu2;

import com.apitechu2.apitechu2.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class Apitechu2Application {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(Apitechu2Application.class, args);

		Apitechu2Application.productModels = Apitechu2Application.getTestData();
	}

	private static  ArrayList<ProductModel> getTestData() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel("1","Producto1",10)
				);

		productModels.add(
				new ProductModel("2","Producto2",20)
						);

		productModels.add(
				new ProductModel("3","Producto3",30)
		);
		return productModels;
	}

}