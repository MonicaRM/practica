package com.apitechu2.apitechu2.controllers;

import com.apitechu2.apitechu2.Apitechu2Application;
import com.apitechu2.apitechu2.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import javax.management.Descriptor;
import java.util.ArrayList;

@RestController
public class ProductController {

    static final String ApiBaseUrl = "/apitechu/v1";

    @GetMapping(ApiBaseUrl+ "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return Apitechu2Application.productModels;
    }

    @GetMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es :" + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : Apitechu2Application.productModels){
            if (product.getId().equals(id)){
                System.out.println("Producto encontrado");
                result = product;
            }

        }

        return result;
    }

    @PostMapping(ApiBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("La descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " +  newProduct.getPrice());

        Apitechu2Application.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en la Url es " + id);
        System.out.println("la id del producto a actualizar es " + product.getId());
        System.out.println("la descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for( ProductModel productInList : Apitechu2Application.productModels){
            if(productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }


    @PatchMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id){
        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar en la Url es " + id);
        System.out.println("la id del producto a actualizar es " + productData.getId());
        System.out.println("la descripción del producto a actualizar es " + productData.getDesc());
        System.out.println("El precio del producto a actualizar es " + productData.getPrice());

        ProductModel result  = new ProductModel();

        for( ProductModel productInList : Apitechu2Application.productModels){
            if(productInList.getId().equals(id)) {
                System.out.println("Producto encontrado ");
                result = productInList;
                if(productData.getDesc() != null) {
                    System.out.println("Actualizo descripción de producto");
                    productInList.setDesc(productData.getDesc());
                }
                if (productData.getPrice() > 0){
                    System.out.println("Actualizo precio del producto");
                    productInList.setPrice(productData.getPrice());
                }
            }
        }

            return result;
    }

    @DeleteMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println(" La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : Apitechu2Application.productModels){
            if( productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if (foundCompany){
            System.out.println("Borrando producto");
            Apitechu2Application.productModels.remove(result);
        }

        return new ProductModel();
    }


}
